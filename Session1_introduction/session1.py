#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 17:38:00 2018

@author: sammi
"""

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import os
import scipy as sp
import scipy.io   #this is needed for the loadmat function

#change this to your working directory (i.e. where the WIN grad programme material is)
wd = '/Users/sammi/Desktop/Experiments/WIN-grad-programme-model'
os.chdir(wd) #change current directory to wd

%matplotlib #this just makes your figures pop out, rather than appear in the console in spyder (like matlab)

#set up a reinforcement learning model

fixedProb       = 0.8  # this is the true probability that green is rewarded
startingProb    = 0.5  # this defines the model's estimated probability on the very first trial
alpha           = 0.05 # this is the model's learning rate
nTrials         = 200  # this is the numper of trials
trueProbability = np.ones(nTrials) * fixedProb #reward probability on each trial

D = sp.io.loadmat('./Session1_introduction/schedule.mat')
trueProbability = D['trueProbabilityStored'][0] # dictionaries are the python equivalent of matlab structures, this is how you index them
#the extra [0] is needed to get the array out of the dictionary properly, as it comes out as a 1x200 array, not a length(200) vector
nTrials = len(trueProbability)

opt1Rewarded    = np.empty(nTrials) #this just needs preloading, as python isnt a fan of randomly creating things

#now we'll simulate whether green was rewarded on every trial
opt1Rewarded[0:nTrials] = np.random.rand(nTrials) < trueProbability 

#plot the schedule on each trial, and the fixed probability of reward
plt.figure()
plt.plot(trueProbability, 'k--', lw = 2, label = 'True Probability')
plt.plot(opt1Rewarded, 'g+', ms = 10, label = 'Trial Outcomes')
plt.xlim(0,nTrials) #sets the x axis
plt.xlabel('Trial Number') #labels it
plt.ylim(0,1) #sets the y axis
plt.ylabel('Green Rewarded?') #labels it
plt.legend() #makes the legend pop up so you can see the labelling for lines!


#here we can write, in line, the RL model function instead of in a separate script.
def RL_model(opt1Rewarded, alpha, startingProb = 0.5):
    
    #by stating startingProb = 0,5, this sets it as a default (unless you change it inside your function call)
    
    #check that alpha has been set appropriately
    if alpha <= 0:
        raise Exception('Learning rate (alpha) must be greater than 0')
    elif alpha >1:
        raise Exception('Learning rate (alpha) must be less than or equal to 1')
    
    #calculate the number of trials
    nTrials = len(opt1Rewarded)

    #pre-create some vectors we're going to assign into
    probOpt1 = np.zeros(nTrials+1, dtype = float)
    delta    = np.zeros(nTrials+1, dtype = float)
    
    #set the first trial's prediction to be equal to the starting probability
    probOpt1[0] = startingProb
    
    #students, complete this code to finish the reinforcement learning model
    delta = np.zeros(nTrials+1, dtype = float)
    for t in range(nTrials):
        delta[t] = opt1Rewarded[t] - probOpt1[t] ### COMPLETE THIS LINE using opt1Rewarded, probOpt1 and equation 1     #prediction error
        probOpt1[t+1] = probOpt1[t] + alpha*delta[t] ### COMPLETE THIS LINE using probOpt1, delta, alpha and equation 2  #prediction for next trial
    return probOpt1
    
#EXERCISE A: complete the last two lines of code in the RL_model function    
probOpt1 = RL_model(opt1Rewarded, alpha, startingProb)
    
# once you've successfully coded your RL_model, you can then plot the model's
# estimated probabilities (on top of the plot above)

plt.plot(probOpt1, 'r-', lw = 2, label = 'RL model probability')
plt.legend() #this will just update the legend


# EXERCISE B. Use the code above to try to understand what happens when you
#   vary alpha (and other parameters). What are the advantages of having a 
#   low alpha? What are the advantages of having a high alpha? If you set
#   alpha to 1, how does the model behave? Make changes to the variables at
#   the top of this cell to explore this (see handout for further details).

# EXERCISE C. Load in the 'reversal' schedule used in the experiment. In
#   which part of the schedule does it help to have a higher alpha? In which
#   part of the schedule does it help to have a lower alpha? (see handout for
#   further details).






#%% How many trials do we look back into the past with different alphas?

alpha = 0.15


T = 25
weight = np.zeros(T)
for t in range(1,T+1):
    #Exercise D. explain/derive the following equation:
    weight[t-1] = (1-alpha)**(T - t)*alpha #only indexing by t-1 here because of zero-indexing in  python
#    weight[t] = np.power(1-alpha, (T-t))*alpha)   #this is just a numpy version of the above line

#plot the weights running back across previous trials    
plt.figure()
plt.plot(np.arange(-(T-1),0+1,1), #np.arange works in (start, stop, step). stop is not inclusive, so add 1 to get zero!
         weight)
plt.ylim(0,alpha*1.2)
plt.xlim(-(T-1), 0)
plt.xlabel('Delay (trials)')
plt.ylabel('Weight')
plt.title('alpha = %0.2f'%alpha)






