#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 08:58:31 2018

@author: sammi
"""

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt



def RLModel(params, subID, data, plot_fits = True):
    
    '''
    params    :  a list (or array-like) of numbers, 1-4 in length. if 2 params, the first is the learning rate, the second is the inverse temperature
                 if 3 params, the first two are learning rates (volatile, stable)
                 if 4 params, 2 learning rates (volatile, stable) and 2 inverse temperatures (volatile, stable)
    
    subID     :  the number of the participant you want to look at
    data      :  the dictionary containing all participant task data
    plot_fits :  bool indicating if you want to plot the model fit (default = True)
    
    '''
    
    #in case we ever want to fit two different learning rates 'alpha',
    #and one or two different temperatures 'beta', make the script flexible and duplicate if only one was specified

    #we have, here, specified a default for plot_fits
    
    if len(params) == 2:
        learningRate = np.array([params[0], params[0]])
        inverseT     = np.array([params[1], params[1]])
    elif len(params) == 3:
        learningRate = np.array([params[0], params[1] ])
        inverseT     = np.array([params[2], params[2] ])
    elif len(params) == 4:
        learningRate = np.array([params[0], params[1]])
        inverseT     = np.array([params[2], params[3]])
        
    
    # define variables for the subject
    subID = subID -1 #because of zero indexing...
    opt1Rewarded    = data['opt1Rewarded'][:,subID]   #whether there was an outcome behind option 1 on each trial
    magOpt1         = data['magOpt1'][:,subID]        #the magnitude of option 1 shown at the time of choice
    magOpt2         = data['magOpt2'][:,subID]
    opt1chosen      = data['opt1Chosen'][:,subID]
    trueProbability = data['trueProbability'][:,subID]
    
    
    #in the matlab script, it needs you to convert from a boolean 0/1 for volatile/stable to 1/2 because of indexing.
    #in python we dont need to do that (the line below shows how you would though)
    #isStable        = np.subtract(2, data['isStableBlock'][:,subID])  #turns 1=stable/0=volatile into: 1=stable, 2 = volatile
    
    #this is the line we really want...
    isStable = data['isStableBlock'][:,subID]
    numtrials       = opt1Rewarded.shape[0]       #number of trials in the experiment
    
    prediction_opt1 = np.zeros(numtrials); 
    rpe             = np.zeros(numtrials);
    
    #assume that the model thinks the reward could be behind either option
    prediction_opt1[0] = 0.5
    
    for t in range(numtrials-1):
        rpe[t] = opt1Rewarded[t] - prediction_opt1[t]
        prediction_opt1[t+1] = prediction_opt1[t] + learningRate[isStable[t]] * rpe[t]
    
    
    #because only one option per trial is rewarded, the reward expectation for option 1 is the opposite to option two
    #both need to sum to 1
    prediction_opt2 = np.subtract(1, prediction_opt1)
    
    if plot_fits:
        plt.figure()
        plt.plot(trueProbability, 'k', label = 'True Probability')
        plt.plot(data['chosenOptionRewarded'][:,subID], 'k*', label = 'Outcomes Received')
        plt.plot(np.multiply(np.add(opt1chosen, 0.05), 0.9), 'r.', label = 'Choice')
        plt.xlabel('Trials')
        plt.ylabel('Reward Probability')
        plt.ylim([-0.1, 1.1])
        plt.plot(prediction_opt1, 'b', label = 'PredictionOpt1')
    
    
    utility1 = np.multiply(magOpt1, prediction_opt1)
    utility2 = np.multiply(magOpt2, prediction_opt2)
    
    #decision variable
    DV = np.subtract(utility2, utility1)
    
    #before the next bit, i'm just going to define a softmax function here to save some time
    
    def softmax(beta, DV):
        out = np.divide(1, 1 + np.exp(-beta*DV))
        return out
    
    # for all trials, calculate the two choice probabilities
    # then save the one of the chosen option as 'probChoice'
    ChoiceProbability1 = np.zeros(numtrials)
    ChoiceProbability2 = np.zeros(numtrials)
    probChoice         = np.zeros(numtrials)
    
    for t in range(numtrials):
        ChoiceProbability2[t] = softmax(beta = inverseT[isStable[t]], DV = DV[t])
        #ChoiceProbability2[t]  = 1 / (1 + np.exp(-inverseT[isStable[t]] * DV[t]))
        ChoiceProbability1[t] = 1 - ChoiceProbability2[t]
        
        if opt1chosen[t] == 1:
            probChoice[t] = ChoiceProbability1[t]
        else:
            probChoice[t] = ChoiceProbability2[t]
    
    if plot_fits:
        plt.plot(ChoiceProbability1, 'g.', label = 'ChoiceProbOpt1')
        plt.legend()
        
    #note that this is different because of zero-indexing!!    
    trials_for_fitting = np.intersect1d(range(numtrials), np.array([np.arange(10,100), np.arange(110,200)]))
    
    #add up the error terms (log likelihood) for the trials used in the fitting
    error = -np.sum( np.log( probChoice[trials_for_fitting] ) )
    
    if np.any(learningRate < 0) or np.any(inverseT < 0) or np.any(learningRate > 1):
        error = 10000
        #raise Exception('check your input parameters: alpha outside of 0-1, or beta is negative')
    
    return error
    
    
    