#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 16:00:53 2018

@author: sammi
"""


import numpy as np
import matplotlib as mpl
from matplotlib import gridspec
from matplotlib import pyplot as plt
import os
import os.path as op
import scipy as sp
import scipy.io   #this is needed for the loadmat function

#change this to your working directory (i.e. where the WIN grad programme material is)
wd = '/Users/sammi/Desktop/Experiments/WIN-grad-programme-model/Session3'
os.chdir(wd) #change current directory to wd

plt.rcParams['figure.figsize'] = [15,10]   #change the default size of figures that we plot in this script
np.set_printoptions(suppress = False) #suppress scientific notation

#this just makes your figures pop out, rather than appear in the console in spyder (like matlab)
# %matplotlib


#%%
# there's a really annoying bug with scipy here that means it doesnt really handle the data we have very well
# but these functions will just help fix this, so we get something more usable out of it!

#note: this was found on stack overflow and works, lol

def loadmat(filename):
    '''
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    '''
    data = sp.io.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)

def _check_keys(dict):
    '''
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    '''
    for key in dict:
        if isinstance(dict[key], sp.io.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict        

def _todict(matobj):
    '''
    A recursive function which constructs from matobjects nested dictionaries
    '''
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, sp.io.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict

D = loadmat('./data.mat')
data = D['data']

#don't ask...

#%% just quickly showing you what is in the data

data.keys() #this will output the contents of the data structure now
data['opt1Rewarded'].shape #you can see that there are 64 people, and 200 trials per person (phew, we're good now...)



#%% 3.0.2 Inspecting individual participant's choices

#select the subject you want to plot

#   NB: zero-indexing means that your first subject is actually subject 0


plotsub = 56;

plt.figure()
plt.title('participant ' + str(plotsub))
plt.plot(data['trueProbability'][:,plotsub],'k', lw = 1, label = 'True Probability')
plt.plot(data['chosenOptionRewarded'][:,plotsub], 'k+', label = 'Outcomes Received')
plt.plot((data['opt1Chosen'][:,plotsub]+0.05)*0.9, 'r.', label = 'Choice')
plt.xlabel('Trials')
plt.ylabel('Reward Probability')
plt.ylim([-0.1, 1.1])
plt.legend()


#%% 3.1.1 Calculating Accuracy

#use a simple definition here, which is whether the participant chose the option with the higher objective *utility*

data['accuracy'] = (np.multiply(data['trueProbability'], data['magOpt1']) > np.multiply((1- data['trueProbability']), data['magOpt2'])) == data['opt1Chosen']

#this ^^ gives you an output of a boolean, so lets just convert this to an integer instead (so we can average, for example)
data['accuracy'] = data['accuracy'].astype(int)

#average across trials (per subject) to get mean accuracy of the individual
data['meanAccuracy'] = np.nanmean(data['accuracy'], 0)

#%% 3.1.2 PLotting accuracy for all/stable/volatile trials

'''
Now we can look at accuracy for just the stable or volatile trials. for that, we'll calculate the mean again
but this time, restricted to the stable and volatile trials as indicated in data['isStableBlock']
'''

#create an empty array for some variables
data['accuracyStable'] = np.full(data['accuracy'].shape, np.nan)
data['accuracyVol']    = np.full(data['accuracy'].shape, np.nan)

rows, columns = np.where(data['isStableBlock'] == 1) #find all the places where in a stable block
data['accuracyStable'][rows,columns] = data['accuracy'][rows,columns]

rows, columns = np.where(data['isStableBlock'] == 0) #find all the places where not in a stable block
data['accuracyVol'][rows, columns] = data['accuracy'][rows, columns]

#average across these trials to get the mean per participant
data['meanAccuracyStable'] = np.nanmean(data['accuracyStable'], 0)
data['meanAccuracyVol']    = np.nanmean(data['accuracyVol'], 0)


bins = np.arange(0.2,1,0.025)
plt.figure()
plt.subplot(3,1,1)
plt.hist(data['meanAccuracyStable'], bins = bins); plt.title('Accuracy stable')
plt.subplot(3,1,2)
plt.hist(data['meanAccuracyVol'], bins = bins); plt.title('Accuracy volatile')
plt.subplot(3,1,3)
plt.hist(data['meanAccuracy'], bins = bins); plt.title('Mean Accuracy')
plt.xlabel('Accuracy'); plt.ylabel('nSubjects')

from scipy.stats import zscore

AccZscored = np.array([zscore(data['meanAccuracyStable']), zscore(data['meanAccuracyVol']), zscore(data['meanAccuracy'])]).T

#heatmap for zscored accuracy
plt.figure()
ax = plt.imshow(AccZscored, aspect = 'auto', vmin = -3, vmax = 3);
plt.colorbar(ax);
plt.title('Colormap for z-scored accuracy')
plt.xlabel('Block Type', size = 14); plt.ylabel('Subjects', size = 14);
plt.xticks([0,1,2], ('Stable', 'Volatile', 'Both'), size = 12)


#%% 3.1.3 Plotting reaction times, mean and variance


# here is a little annoyance about plotting in matplotlib:
# if you want have a plot occupy multiple panels of a subplot figure, you need to specify a GridSpec
# you give it a shape, and then instead of e.g. plt.subplot(2,2, 1:2) you have to call plt.subplot(gs[0,0:2]) 
# remember that we index from 0 in python (so 3 rows are numbered 0,1,2) and range(0:2) actually only gives you [0,1] as output
# this explains what may seem like weird indexing of the subplot!

plt.figure()
gs = mpl.gridspec.GridSpec(2,3)

plt.subplot(gs[0,0:2])
plt.plot(np.nanmean(data['reactionTime'], 0)); plt.title('mean RT'); plt.ylabel('meanRT'); plt.ylim([0.5,2])

plt.subplot(gs[1,0:2])
plt.plot(np.std(data['reactionTime'],0)); plt.title('std RT'); plt.ylabel('std RT'); plt.ylim([0,8])
plt.xlabel('Subjects')

plt.subplot(gs[0,2])
plt.hist(np.mean(data['reactionTime'], 0), orientation = 'horizontal', bins = np.arange(0.5,2,0.1))
plt.ylim([0.5,2]); plt.title('histogram')

plt.subplot(gs[1,2])
plt.hist(np.std(data['reactionTime'], 0), orientation = 'horizontal', bins = np.arange(0,8,0.5));
plt.ylim([0,8]); plt.xlabel('nSubj')


#zscore the mean RT and std RT data (zscore function outputs a numpy array so dont worry about making it an array)
meanRTZscored = zscore(np.mean(data['reactionTime'], 0))
stdRTZscored  = zscore(np.std(data['reactionTime'],  0))

#%% 3.1.4 Identify outliers using the standard deviation

discardThresh = 3;



data['discardSubj'] = np.sum(np.vstack([np.abs(AccZscored).T, np.abs(meanRTZscored), np.abs(stdRTZscored)]).T > discardThresh, 1) > 0


#check how many subjects were excluded with the threshold (this is basically the equivalent of find)
np.where(data['discardSubj']) #by default, np.where will look for a True boolean. you can specify a condition though if you want

plt.figure()
plt.plot(AccZscored)
plt.plot(meanRTZscored)
plt.plot(stdRTZscored)
plt.plot(np.squeeze(np.where(data['discardSubj'])), np.multiply(np.ones(np.squeeze(np.where(data['discardSubj'])).shape[0]), 3), 'ro')
plt.xlabel('Subjects'); plt.ylabel('zscore')
plt.legend(labels = ['Accuracy Stable', 'Accuracy Vol', 'Accuracy All', 'mean RT', 'std RT', 'outliers'])


#%% Win-stay/Lose-stay


#here we'll first calculate whether subjects stayed after a win or loss

Stay = np.multiply(np.ones(data['accuracy'].shape, dtype = int), -100) #i'm just going to initialise an empty array with an arbitrary, infeasible number in each cell
Stay[0,:] = np.nan

Stay = np.vstack([ np.full([1,data['accuracy'].shape[1]],np.NaN), np.array(np.squeeze([data['opt1Chosen'][0:-1] == data['opt1Chosen'][1:]]), dtype = int)])

LastTrialReward = np.vstack([  np.full([1,data['accuracy'].shape[1]],np.NaN), data['chosenOptionRewarded'][:-1]])

#create some empty vectors here
WinStay     = np.zeros(LastTrialReward.shape[1])
LoseStay    = np.zeros(LastTrialReward.shape[1])
WinStayVol  = np.zeros(LastTrialReward.shape[1])
LoseStayVol = np.zeros(LastTrialReward.shape[1])
WinStayStb  = np.zeros(LastTrialReward.shape[1])
LoseStayStb = np.zeros(LastTrialReward.shape[1])

for a in range(LastTrialReward.shape[1]):
    WinStay[a]  = np.mean(Stay[LastTrialReward[:,a] == 1,a])
    LoseStay[a] = np.mean(Stay[LastTrialReward[:,a] == 0,a])

    WinStayVol[a]  = np.mean(Stay[np.where(np.logical_and(LastTrialReward[:,a] == 1, data['isStableBlock'][:,a] == 0)),a])
    LoseStayVol[a] = np.mean(Stay[np.where(np.logical_and(LastTrialReward[:,a] == 0, data['isStableBlock'][:,a] == 0)),a])
    WinStayStb[a]  = np.mean(Stay[np.where(np.logical_and(LastTrialReward[:,a] == 1, data['isStableBlock'][:,a] == 1)),a])
    LoseStayStb[a] = np.mean(Stay[np.where(np.logical_and(LastTrialReward[:,a] == 0, data['isStableBlock'][:,a] == 1)),a])

#plot win/stay on average across stable and volatile


plt.figure()
plt.subplot(1,3,1)
plt.bar(['Win Stay', 'Lose Stay'], [np.mean(WinStay), np.mean(LoseStay)], fc = [0.2, 0.3, 0.7])
plt.errorbar(x    = ['Win Stay', 'Lose Stay'],
             y    = [np.mean(WinStay), np.mean(LoseStay)],
             yerr = np.power([np.divide(np.std(WinStay),len(WinStayVol)), np.divide(np.std(LoseStay),len(WinStayVol))], .5))
plt.ylim([0.3,1])
plt.ylabel('Probability of staying')
plt.title('All', size = 16)

plt.subplot(1,3,2)
plt.bar(['Win Stay', 'Lose Stay'], [np.mean(WinStayVol), np.mean(LoseStayVol)], fc = [0.2, 0.3, 0.7])
plt.errorbar(x    = ['Win Stay', 'Lose Stay'],
             y    = [np.mean(WinStayVol), np.mean(LoseStayVol)],
             yerr = np.power([np.divide(np.std(WinStayVol),len(WinStayVol)), np.divide(np.std(LoseStayVol),len(WinStayVol))], .5))
plt.ylim([0.3,1])
plt.ylabel('Probability of staying')
plt.title('Volatile', size = 16)

plt.subplot(1,3,3)
plt.bar(['Win Stay', 'Lose Stay'], [np.mean(WinStayStb), np.mean(LoseStayStb)], fc = [0.2, 0.3, 0.7])
plt.errorbar(x    = ['Win Stay', 'Lose Stay'],
             y    = [np.mean(WinStayStb), np.mean(LoseStayStb)],
             yerr = np.power([np.divide(np.std(WinStayStb),len(WinStayStb)), np.divide(np.std(LoseStayStb),len(WinStayStb))], .5))
plt.ylim([0.3,1])
plt.ylabel('Probability of staying')
plt.title('Stable', size = 16)

# perform some basic t-tests here
sp.stats.ttest_rel(WinStayVol, WinStayStb)
sp.stats.ttest_rel(LoseStayVol, LoseStayStb)

#%%  3.2 MODEL FITTING USING GRID SEARCH

#%% 3.2.1 Developing and intuition for the goodness of a fit

'''
in this script, we're going to have our function callable in a different way.
in session 1, we created an RL model function within the script
e.g. def RLmodel(params): ....
here, what we'll do is have this in a separate script, and load that script in like a toolbox.
I've called this helperfunctions.py
'''
from helperfuncs import RLModel

#this function requires the following parameters to be set:
# params:     parameters for your model, learning rate (alpha) and inverse temperature (beta)
# subID:      as it will index the subject you want, from the
# data:       data dictionary (what we're dealing with here) that you provide
# plot_fits:  by default, this is set to true, and will output your model fit

subjN = 12

#fit one learning rate and one inverse temperature
error = RLModel(params = [0.15, 0.02], subID = subjN, data = data)


#if you want to try fitting two learning rates, uncomment this line
# error = RLModel(params = [0.1, 0.3, 0.05], subID = subjN, data = data)

#btw, you can also just call it without saving to a variable, if you want to try something else without overwriting a variable, e.g.
# RLModel(params = [0.1, 0.05, 0,.05], subID = 22, data = data)


#%% 3.2.2 Grid Search

#choose a subject here
subjN = 13

#lets set up the grid. you can make the grid finer by making step sizes smaller, or coarser by making them larger.
#the finer the grid, the longer it will take to compute values
gridLearningRate = np.arange(0.1, 1.1, 0.1)    #start, end, step
gridInverseT     = np.arange(0.01, 0.3, 0.03)

#we'll run two for-loops over all alphas then betas, and save the logLL (error) for each

logLLGrid = np.zeros([gridLearningRate.size, gridInverseT.size])

for alpha in range(len(gridLearningRate)):
    learn_rate = gridLearningRate[alpha]
    for beta in range(len(gridInverseT)):
        invT = gridInverseT[beta]
        logLLGrid[alpha,beta] = RLModel([learn_rate,invT], subID = subjN, data = data, plot_fits = False)
        
#this will just plot the error values we got for all combinations of alpha and beta in the grid search above, as a colour scale    
plt.figure()
plt.imshow(logLLGrid,# interpolation = 'bilinear', #interpolation will just smooth it so it looks pretty
           extent = [gridLearningRate[0], gridLearningRate[-1], gridInverseT[0], gridInverseT[-1]])
plt.ylabel('Learning rate (alpha)')
plt.xlabel('Inverse softmax temperature (beta)')
plt.title('subject ' + str(subjN))
plt.colorbar()


#%% 3.2.3 Grid search seaparetly for volatile and stable
 
gridLearningRate = np.arange(0.1,1,0.2)
gridInverseT     = np.arange(0.01, 0.3, 0.06)
subjN = 13

parast    = np.array([np.tile(gridLearningRate,(2,1)), np.tile(gridInverseT,(2,1))]).flatten().reshape(4,5)
params    = np.array(np.meshgrid(parast[0], parast[1], parast[2], parast[3])).T.reshape(-1,4)

#this will make the grid, but its in a weird order
#this will create an array of indices that lets us reshape this so its more like what CombVec does in matlab
#this isn't really needed though, because you find where the grid is at a minimum, so it doesnt matter the order
reshaping = np.lexsort((params[:,3], params[:,2], params[:,1]))
params = params[reshaping]
del(reshaping) #just clear it as no longer needed

logLLGrid = np.zeros((params.shape[0], 1))
for pCombi in range(params.shape[0]):
    logLLGrid[pCombi] = RLModel(params = params[pCombi,:], subID = subjN, data = data, plot_fits = False)

minVal = np.where(logLLGrid == np.min(logLLGrid))[0] #get just the row
params[minVal,:] #this gives you the volatile LR, stable RT, volatile invT, stable invT
#in the code for the RLModel, isStable == 0 means volatile, and isStable == 1 means stable
# so the output params are LR[isStable == 0], LR[isStable == 1], invT[isStable == 0], invT[isStable == 1]


#plotting the grid again for volatile and stable separately

logLLGrid2 = logLLGrid.reshape(len(gridInverseT), len(gridInverseT), len(gridLearningRate), len(gridLearningRate))
params2    = params.reshape(len(gridLearningRate), len(gridLearningRate), len(gridInverseT), len(gridInverseT), params.shape[1])

plt.figure()
plt.subplot(1,2,1) #stable
plt.title('Stable')
plt.imshow(np.squeeze(np.min(np.min(logLLGrid2,3),1)))
plt.xticks(range(5),['0.01','0.07','0.13','0.19','0.25'] ) #invT
plt.yticks(range(5), ('0.1','0.3','0.5','0.7','0.9')) #LR
plt.xlabel('Beta Value')
plt.ylabel('Alpha Value')


plt.subplot(1,2,2) #volatile
plt.title('Volatile')
plt.imshow(np.squeeze(np.min(np.min(logLLGrid2,2),0)))
plt.xticks(range(5),['0.01','0.07','0.13','0.19','0.25'] ) #invT
plt.yticks(range(5), ('0.1','0.3','0.5','0.7','0.9')) #LR
plt.xlabel('Beta Value')
plt.ylabel('Alpha Value')

#looking at the graph you can find the 'best fitting' learning rate and temperature beta for stable/volatile
#and compare this with the one you derived numerically above

#%% 3.2.4 Grid search for all subjects, separately for volatile and stable
#this takes a little time to run

gridLearningRate = np.arange(0.1, 1, 0.2)
gridInverseT     = np.arange(0.01, 0.3, 0.06)

parast    = np.array([np.tile(gridLearningRate,(2,1)), np.tile(gridInverseT,(2,1))]).flatten().reshape(4,5)
params    = np.array(np.meshgrid(parast[0], parast[1], parast[2], parast[3])).T.reshape(-1,4)


reshaping = np.lexsort((params[:,3], params[:,2], params[:,1]))
params    = params[reshaping]
del(reshaping) #just clear it as no longer needed

nsubs = data['discardSubj'].size
minvals    = np.zeros(data['discardSubj'].size, dtype = int)
bestparams = np.zeros((data['discardSubj'].size, params.shape[1]))

logLLGrid = np.zeros((nsub, params.shape[0]))

for sub in np.arange(nsubs):
    subj = sub + 1
    for pCombi in range(params.shape[0]):
        logLLGrid[sub, pCombi] = RLModel(params = params[pCombi,:], subID = subj, data = data, plot_fits=False)
    
    minvals[sub] = np.where(logLLGrid[sub,:] == min(logLLGrid[sub,:]))[0]
    bestparams[sub,:] = params[minvals[sub],:]

#bestparams = bestparams[data['discardSubj'] == False,:] #filter out the discarded subjects

titles = ['LR volatile', 'LR stable', 'Beta volatile', 'Beta stable']
plt.figure()
for i in range(bestparams.shape[1]):
    plt.subplot(2,2,i+1)
    plt.hist(bestparams[:,i], np.arange(0,1,0.05))
    plt.xlim([0,1])
    plt.ylim([0,50])
    plt.title(titles[i])

meanBestParam = np.mean(bestparams, 0)

#%% 3.3 MODEL FITTING USING FMINSEARCH

#%% 3.3.1 Fitting two learning rates and one beta

#import the python fminsearch algorithm, and just rename it for compatibility

from scipy.optimize import fmin as fminsearch

nsubs = data['discardSubj'].size

alpha_init = np.array([0.1,  np.mean(meanBestParam[0:2]), 0.6 ])
beta_init  = np.array([0.01, np.mean(meanBestParam[2:]) , 0.1 ])

parast = np.vstack([np.tile(alpha_init, (2,1)), np.tile(beta_init, (1,1))])
params = np.array(np.meshgrid(parast[0], parast[1], parast[2])).T.reshape(-1,3)
parfitall = np.zeros((nsubs, params.shape[0],params.shape[1]))
negLogLL  = np.zeros((nsubs, params.shape[0]))

parfit3   = np.zeros((nsubs, params.shape[1]))
negLogLL3 = np.zeros((nsubs))

for sub in range(nsubs):
    print('sub %d/%d'%(sub+1, nsubs))
    subj = sub + 1
    for ip in range(params.shape[0]):
        out  = fminsearch(func = RLModel, x0 = params[ip,:], args = (subj,data,False), maxiter = 10000, maxfun  = 10000,
                          full_output = True, disp = False)
        
        parfitall[sub,ip,:] = out[0]
        negLogLL[sub,ip]    = out[1]
    
    minErr  = np.where(negLogLL[sub,:] == np.min(negLogLL[sub,:]))[0]
    parfit3[sub,:] = np.squeeze(parfitall[sub, minErr, :])
    negLogLL3[sub] = negLogLL[sub, minErr]

    
data['LearningRateVolatile']  = parfit3[:,0]
data['LearningRateStable']    = parfit3[:,1]
data['InverseTemperature3']   = parfit3[:,2]

sp.io.savemat(file_name = 'dataFitted2', mdict = data, appendmat = True)
np.where(data['discardSubj'] == False)
#plot simple bar graph of the learning rates
subs2use_inds = np.where(data['discardSubj']==False)

plt.figure()
plt.bar(['Learning Rate Stable', 'Learning Rate Volatile'], [np.mean(data['LearningRateStable']), np.mean(data['LearningRateVolatile'])], fc = [.5,.5,.5], width = .5)
plt.errorbar(x = ['Learning Rate Stable', 'Learning Rate Volatile'],
             y = [np.mean(data['LearningRateStable']), np.mean(data['LearningRateVolatile'])],
             yerr = np.power([np.divide(np.std(data['LearningRateStable']),len(data['LearningRateStable'])), np.divide(np.std(data['LearningRateVolatile']),len(data['LearningRateVolatile']))], .5))
plt.plot([0,1],[data['LearningRateStable'], data['LearningRateVolatile']], 'k-', lw = .2)
plt.ylim([0,1])
plt.title('Learning Rate')

t, p = sp.stats.ttest_rel(data['LearningRateStable'], data['LearningRateVolatile'])


#%% 3.4 MODEL COMPARISON
#%% 3.4.1 Fitting one learning rate and one beta


#same as the previous cell, but we're just going to fit one learning rate and one beta
nsubs = data['discardSubj'].size

alpha_init = np.array([0.1,  np.mean(meanBestParam[0:1]), 0.6 ])
beta_init  = np.array([0.01, np.mean(meanBestParam[2:]) , 0.1 ])

parast     = np.vstack([np.tile(alpha_init, (1,1)), np.tile(beta_init, (1,1))])
params     = np.array(np.meshgrid(parast[0], parast[1])).T.reshape(-1,2)
parfitall  = np.zeros((nsubs, params.shape[0],params.shape[1]))
negLogLL   = np.zeros((nsubs, params.shape[0]))

parfit2    = np.zeros((nsubs, params.shape[1]))
negLogLL2  = np.zeros((nsubs))

for sub in range(nsubs):
    print('sub %d/%d'%(sub+1, nsubs))
    subj = sub + 1
    for ip in range(params.shape[0]):
        out  = fminsearch(func = RLModel, x0 = params[ip,:], args = (subj,data,False), maxiter = 10000, maxfun  = 10000,
                          full_output = True, disp = False)
        
        parfitall[sub,ip,:] = out[0]
        negLogLL[sub,ip]    = out[1]
    
    minErr  = np.where(negLogLL[sub,:] == np.min(negLogLL[sub,:]))[0]
    parfit2[sub,:] = np.squeeze(parfitall[sub, minErr, :])
    negLogLL2[sub] = negLogLL[sub, minErr]

    
data['LearningRateBoth']    = parfit2[:,0]
data['InverseTemperature2'] = parfit2[:,1]
sp.io.savemat(file_name = 'dataFitted2', mdict = data, appendmat = True)


#%% 3.4.2 Computing the AIC for model comparison

# Here we will compute the Akaike Information Criterion (AIC). Computing
# the AIC values is really simple:
# We need to know the negative log likelihood we got back from fminsearch 
# and we need to know the number of parameters in our model, which was 2 
# and 3, respectively.


#AIC for the 2 parameter model
nparam   = 2
AIC2     = 2*np.sum(negLogLL2) + 2*nparam
AIC2_all = np.add(np.multiply(2, negLogLL2), 2*nparam)

#AIC for the 3 parameter model
nparam   = 3
AIC3     = 2*np.sum(negLogLL3) + 2*nparam
AIC3_all = np.add(np.multiply(2, negLogLL3), 2*nparam)
negLogLL3
print 'AIC values: %f   %f' %(AIC2, AIC3)

AIC_comp = np.subtract(AIC3_all,AIC2_all)

plt.figure()
plt.bar(np.arange(nsubs),AIC_comp)
plt.title('AIC for 3param-2param (neg: 3param is better, pos: 2param better)')

#how much better is the 3param vs. 2 param model?
#compute the relative likeligood


relLL3 = np.exp((AIC3-AIC2)/2)
print 'Relative likelihood: %s'%(str(relLL3))
