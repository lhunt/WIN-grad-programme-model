data {
  int<lower=1> N;
  real y[N];
}

parameters {
  real y_mu;
  real<lower=0> y_sd;
}

model {
  y ~ normal(y_mu,y_sd);
}
