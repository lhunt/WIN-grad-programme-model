% First, set the folder containing this script as the 'current folder'
% (e.g. using Matlab address field in current folder toolbar, or 'cd'
% command, or click on 'change folder' when prompted)
% Second, run this script to see whether you have installed Stan correctly

% If there are errors that Matlab does not know 'stan' or 'process
% manager', make sure that these folders (that you should have installed
% from the links described in the help document) are added to your matlab
% path (in the Matlab main window, click on 'set path')
nchains = 4;
niter    = 500;
nwarmup  = 500;

% Generate some random data
y = normrnd(10,3,[1,40]);
inp.y = y;
inp.N = length(y);

% Model file and path
mpath = fullfile(pwd);
mname = 'StanTest';
wdir = fullfile(mpath,'outputs',mname); if exist(wdir,'dir')~=7;  mkdir(wdir);end
mfile=fullfile(mpath, [mname '.stan']);

% Run Stan
StFit= stan('file',mfile,'data',inp,'iter',niter,'warmup',nwarmup,'chains',nchains,...
            'verbose',true,'working_dir',wdir);
% Pauses the matlab code until the model is fit
StFit.block(); 
% Extract the output from Stan as summary table
SFsummary   = StFit.print;