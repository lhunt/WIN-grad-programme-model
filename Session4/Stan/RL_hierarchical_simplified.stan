// 'Simplified': we pretend that the bounded parameters are normally distributed at the group level to make the code easier to understand for now
// we also ignore that we should test for effects within-subject than between-subjects
// and the code is not optimized for running well

// The 'data' block list all input variables that are given to Stan from Matlab. You need to specify the size of the arryas
data {
  int ntr;                    // number of trials per participant "int" means that the values are integers
  int nsub;                   // number of subjects
  int opt1Chosen[ntr,nsub];   // whether option 1 was chosen on each trial, "[ntr,nsub]" defines the size of the arrya
  int opt1Rewarded[ntr,nsub]; // whether option 1 was rewarded on the trial or not
  int magOpt1[ntr,nsub];      // reward magnitude of option 1
  int magOpt2[ntr,nsub];      // reward magnitude of option 2
  int isStableBlock[ntr,nsub];// for each trial, whether it belongs to the stable or the volatile block. This is needed for selecting which learning rate should be used
  int includeTrial[ntr];      // whether the data from this trial should be fitted (we exclude the first 10 trials per block)
}

// The 'parameters' block defines the parameter that we want to fit
parameters {
  // Group level parameters
  real<lower=0,upper=1> alpha_mu[2]; //group level mean for learning rate
  real<lower=0 > alpha_sd[2]; // group level standard deviation for learning rate
  real<lower=0> beta_mu; // group level mean for inverse temperature
  real<lower=0> beta_sd; // group level standard deviation for inverse temperature
  // Single subject parameters (as before), but now transformed
  real<lower=0,upper=1> alpha[nsub,2]; // learning rate - separate learning rates for stable and volatile; two per participant
  real<lower=0> beta[nsub];    // inverse temperature (i.e. how consistent choices are); one per participant
}

// This block runs the actual model - as before, apart from changes to the prior settings
model {
  // temporary variables that we will compute for each person and each trial
  real predictionOpt1[ntr,nsub];  //prediction how likely option 1 is to be rewarded
  real predictionError[ntr,nsub]; // prediction error
  real utility1[ntr,nsub];        // utility of option 1
  real utility2[ntr,nsub];        // utility of option 2


  // Priors - define the likely values for the group level parameters
  alpha_sd ~ normal(0,0.5);
  beta_sd  ~ normal(0,0.5);
  beta_mu  ~ normal(0,0.5);

  // Priors for the individual subjects are the group:
  for (is in 1:nsub){
    alpha[is,1] ~ normal(alpha_mu[1],alpha_sd[1]);
    alpha[is,2] ~ normal(alpha_mu[2],alpha_sd[2]);
    beta[is]    ~ normal(beta_mu,beta_sd);
  }

 // running the model is as before:
  for (is in 1:nsub){ // run the model for each subject
    // Learning
    predictionOpt1[1,is] = 0.5; // on the first trial, 50-50 is the best guess
    for (it in 1:(ntr-1)){
      predictionError[it,is]  = opt1Rewarded[it,is]-predictionOpt1[it,is];
      predictionOpt1[it+1,is] = predictionOpt1[it,is] + alpha[is,isStableBlock[it,is]]*(predictionError[it,is]);
    }
    // Decision - combine predictions of reward probability with magnitudes
    for (it in 1:ntr){
      if ((opt1Chosen[it,is]!=999) && includeTrial[it]==1){ // if there is no missing response
        // Utility
        utility1[it,is] = predictionOpt1[it,is]*magOpt1[it,is];     // option 1: probability x magnitude
        utility2[it,is] = (1-predictionOpt1[it,is])*magOpt2[it,is]; // option 2: probability x magnitude

        // Compare the choice probability (based on the utility) to the actual choice
        // See the handout for the syntax of the bernoulli_logit function
        // equivalently we could have written (as we have done previously in Matlab; but this runs a bit less well in Stan).:
        // ChoiceProbability1[it,is] = 1/(1+exp(beta[is]*(util2[it,is]-util1[it,is]))); // the softmax is an 'inv_logit'
        // opt1Chosen[it,is] ~ bernoulli(ChoiceProbability1[it,is]);
        opt1Chosen[it,is] ~ bernoulli_logit(beta[is]*(utility1[it,is]-utility2[it,is])); // bernoulli is a distribution in the way as e.g. the 'normal distribution'
      }
    }
  }
}
