function out =  logit(inp)
out = log(inp./(1-inp));