%% File paths, stan settings, load data
clear

basepath = '/Users/JScholl/Documents/Teaching/WINcomputationalMay2018/Session4/Stan/'; % folder with the scripts and the data
load(fullfile(basepath,'dataFitted.mat'));
addpath(genpath(basepath)) % add all the scripts to Matlab directory

% Stan settings
nchains = 3;
niter   = 3000;
fitmethod='variational';
nsub= size(data.opt1Chosen,2);
%% Reshape data for Stan
% the call to the Stan software from Matlab takes one structure as input -
% for easier reading, i'm copying only the relevant fields from 'data' to
% 'inp'
inp.ntr=200;                            % number of trials per subject
inp.nsub=nsub;                          % number of subjects
inp.opt1Chosen      = data.opt1Chosen;  % whether subject chose option 1, format: number of trials x number of subjects
inp.opt1Rewarded    = data.opt1Rewarded;% whether option 1 was rewarded, format: number of trials x number of subjects
inp.magOpt1         = data.magOpt1;     % the reward magnitude of option 1, shown on the screen at the time of choice; format: n trials x n subjects
inp.magOpt2         = data.magOpt2;     % reward magnitude for option 2
inp.isStableBlock   = 2-data.isStableBlock; % whether we are in the stable block (=1) or volatile block (=2) - this is needed for selecting the appropriate learning rate in the code later; format; ntr x nsubs
inp.includeTrial    = ones(inp.ntr,1);  % we only fit the model from trial 11 onwards in each block (because e.g. in the stable block, learning rate according to a Bayesian model should actually be high), so this is an index whether we should use a trial for fitting (=1) or not (=0); format: ntr x nsubs
inp.includeTrial([1:10 101:110]) = 0;

%% Run stan - no hierarchy
mname = 'RL_independent_subjects';
clear out
% Make a folder to store the temporary files stan creates
wdir = fullfile(basepath,'outputs',mname); if exist(wdir,'dir')~=7;  mkdir(wdir);end
mfile=fullfile(basepath, [mname '.stan']);

% Run the Stan model
StFit= stan('file',mfile,'data',inp,'iter',niter,'warmup',niter,'chains',nchains,...
            'verbose',true,'working_dir',wdir,'method',fitmethod);
        
% Pauses the matlab code until the model is fit
StFit.block(); 
% Extract the output from Stan as summary table
SFsummary   = StFit.print;
% Extract the samples for each parameter (combining across the different chains)
ParasPerm = StFit.extract('permuted',true);
% Extract the samples, keeping the chain separate
ParasNPerm = StFit.extract('permuted',false);
% Extract the samples, combining across chains
out.ParasPerm=ParasPerm;
out.ParasNPerm=ParasNPerm;
out.data = data;
out.summary = SFsummary;
save(fullfile(basepath,['Stan_' mname '_' fitmethod]),'out')
%% Visualize the data - what do credible intervals mean?
figure('color','w')
plotSub=10;
% Histogram of single parameters - alpha stable
subplot(2,3,1)
myDat=out.ParasPerm.alpha(:,plotSub,1);
histD=histogram(myDat);
sortDat = sort(myDat);
% add credible intervals
CI95 = sortDat([round(0.025*length(sortDat)) round(0.975*length(sortDat))]);
hold on 
line([CI95(1) CI95(1)],[0 max(histD.Values)],'color','red')
line([CI95(2) CI95(2)],[0 max(histD.Values)],'color','red')
title(['Subject ' num2str(plotSub)])
ylabel('Number of samples')
xlabel('Alpha (stable)')
% Histogram of single parameters - alpha volatile
subplot(2,3,2)
myDat=out.ParasPerm.alpha(:,plotSub,2);
histD=histogram(myDat);
sortDat = sort(myDat);
% add credible intervals
CI95 = sortDat([round(0.025*length(sortDat)) round(0.975*length(sortDat))]);
hold on 
line([CI95(1) CI95(1)],[0 max(histD.Values)],'color','red')
line([CI95(2) CI95(2)],[0 max(histD.Values)],'color','red')
ylabel('Number of samples')
xlabel('Alpha (volatile)')
% Histogram of single parameters - beta
subplot(2,3,3)
myDat=out.ParasPerm.beta(:,plotSub);
histD=histogram(myDat);
sortDat = sort(myDat);
% add credible intervals
CI95 = sortDat([round(0.025*length(sortDat)) round(0.975*length(sortDat))]);
hold on 
line([CI95(1) CI95(1)],[0 max(histD.Values)],'color','red')
line([CI95(2) CI95(2)],[0 max(histD.Values)],'color','red')
ylabel('Number of samples')
xlabel('Beta')

% Scatter plots showing all three parameters
subplot(2,3,4)
scatter(out.ParasPerm.alpha(:,plotSub,1),out.ParasPerm.alpha(:,plotSub,2))
xlabel('Alpha stable')
ylabel('Alpha volatile')
subplot(2,3,5)
scatter(out.ParasPerm.alpha(:,plotSub,1),out.ParasPerm.beta(:,plotSub))
xlabel('Alpha stable')
ylabel('Beta')
subplot(2,3,6)
scatter(out.ParasPerm.alpha(:,plotSub,2),out.ParasPerm.beta(:,plotSub))
xlabel('Alpha volatile')
ylabel('Beta')
%% Compare model fits from Matlab to those from Stan
load(fullfile(basepath,'Stan_RL_independent_subjects_variational'))
figure('color','w')
subplot(3,1,1)
scatter(mean((out.ParasPerm.alpha(:,:,1))),(out.data.LearningRateStable))
xlabel('Stan Parameters')
ylabel('Matlab Parameters')
title('Learning rate stable')

subplot(3,1,2)
scatter(mean(out.ParasPerm.alpha(:,:,2)),out.data.LearningRateVolatile)
xlabel('Stan Parameters')
ylabel('Matlab Parameters')
title('Learning rate volatile')

subplot(3,1,3)
scatter(mean(out.ParasPerm.beta),out.data.InverseTemperature3)
xlabel('Stan Parameters')
ylabel('Matlab Parameters')
title('Inverse temperature')
%% Visualize the data - histogram across participants
load(fullfile(basepath,'Stan_RL_independent_subjects_variational'))
figure('color','w')
% Plot the untransformed data
subplot(2,3,1)
histogram(squeeze(mean(out.ParasPerm.alpha(:,:,1))),0:0.05:0.8)
ylabel('Number of participants')
xlabel('Learning rate (stable)')
title('Histogram across participants')
subplot(2,3,2)
histogram(squeeze(mean(out.ParasPerm.alpha(:,:,2))),0:0.05:0.8)
ylabel('Number of participants')
xlabel('Learning rate (volatile)')
title('Histogram across participants')
subplot(2,3,3)
histogram(squeeze(mean(out.ParasPerm.beta)),0:0.05:0.8)
ylabel('Number of participants')
xlabel('Inverse temperature')
title('Histogram across participants')

% Plot the transformed data
subplot(2,3,4)
histogram(squeeze(mean(logit(out.ParasPerm.alpha(:,:,1)))))
ylabel('Number of participants')
xlabel('Learning rate (stable)')
title('Logit transformed')

subplot(2,3,5)
histogram(squeeze(mean(logit(out.ParasPerm.alpha(:,:,2)))))
ylabel('Number of participants')
xlabel('Learning rate (volatile)')
title('Logit transformed')

subplot(2,3,6)
histogram(squeeze(mean(log(out.ParasPerm.beta))))
ylabel('Number of participants')
xlabel('Inverse temperature')
title('Log transformed')



%% Run stan - with hierarchy
mname = 'RL_hierarchical_simplified';%'RL1Hierarchical_Simplified';

clear out
% Make a folder to store the temporary files stan creates
wdir = fullfile(basepath,'outputs',mname); if exist(wdir,'dir')~=7;  mkdir(wdir);end
mfile=fullfile(basepath, [mname '.stan']);

% Run the Stan model
StFit= stan('file',mfile,'data',inp,'iter',niter,'warmup',niter,'chains',nchains,...
            'verbose',true,'working_dir',wdir,'method',fitmethod);
% Pauses the matlab code until the model is fit
StFit.block(); 
% Extract the output from Stan as summary table
SFsummary   = StFit.print;
% Extract the samples for each parameter (combining across the different chains)
ParasPerm = StFit.extract('permuted',true);
% Extract the samples, keeping the chain separate
ParasNPerm = StFit.extract('permuted',false);

out.ParasPerm=ParasPerm;
out.ParasNPerm=ParasNPerm;
out.data = data;
out.summary = SFsummary;
save(fullfile(basepath,['Stan_' mname '_' fitmethod]),'out')


%% Comparing hierarchical to non-hierarchical fit
clear out
load(fullfile(basepath,'Stan_RL1_sample'))
out1=out; clear out
load(fullfile(basepath,'Stan_RL1HierarchicalWithinSubject_sample'))
out2=out; clear out
figure('color','w')
subplot(3,1,1)
scatter(mean((out1.ParasPerm.alpha(:,:,1))),mean((out2.ParasPerm.alpha(:,:,1))))
xlabel('Nonhierarchical')
ylabel('Hierarchical')
title('Learning rate stable')

subplot(3,1,2)
scatter(mean((out1.ParasPerm.alpha(:,:,2))),mean((out2.ParasPerm.alpha(:,:,2))))
xlabel('Nonhierarchical')
ylabel('Hierarchical')
title('Learning rate volatile')

subplot(3,1,3)
scatter(mean(out1.ParasPerm.beta),mean(out2.ParasPerm.beta))
xlabel('Nonhierarchical')
ylabel('Hierarchical')
title('Inverse temperature')
%% Visualizing results from a hierarchical model fit
% In Stan, we have a whole distribution for the group level. One way to
% visualize is to plot a histogram with 95% credible intervals
load(fullfile(basepath,'Stan_RL1Hierarchical_Simplified_variational'))
figure('color','w')
for ip = 1:4
    if ip == 1
        myDat = out.ParasPerm.alpha_mu(:,1);
        myTitle='Alpha stable';
    elseif ip==2
        myDat = out.ParasPerm.alpha_mu(:,2);
        myTitle='Alpha volatile';
    elseif ip==3
        myDat = out.ParasPerm.beta_mu;
        myTitle='Inverse temperature';
    elseif ip==4
        myDat = out.ParasPerm.alpha_mu(:,1)-out.ParasPerm.alpha_mu(:,2);
        myTitle='Alpha stable-volatile';
    end
    subplot(1,4,ip)
    histD=histogram(myDat,10);
    sortDat = sort(myDat);
    CI95 = sortDat([round(0.025*length(sortDat)) round(0.975*length(sortDat))]);
    hold on 
    line([CI95(1) CI95(1)],[0 max(histD.Values)],'color','red')
    line([CI95(2) CI95(2)],[0 max(histD.Values)],'color','red')
    ylabel('samples')
    title(myTitle)
end

