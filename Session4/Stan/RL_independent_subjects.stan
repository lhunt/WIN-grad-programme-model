// The 'data' block list all input variables that are given to Stan from Matlab. You need to specify the size of the arryas
data {
  int ntr;                    // number of trials per participant "int" means that the values are integers
  int nsub;                   // number of subjects
  int opt1Chosen[ntr,nsub];   // whether option 1 was chosen on each trial, "[ntr,nsub]" defines the size of the arrya
  int opt1Rewarded[ntr,nsub]; // whether option 1 was rewarded on the trial or not
  // reward magnitude of option 1
  // reward magnitude of option 2
  int isStableBlock[ntr,nsub];// for each trial, whether it belongs to the stable or the volatile block. This is needed for selecting which learning rate should be used
  int includeTrial[ntr];      // whether the data from this trial should be fitted (we exclude the first 10 trials per block)
}


// The 'parameters' block defines the parameter that we want to fit
parameters {
  // Stan syntax explanation:
  // real : parameters are real numbers
  // <lower=0,upper=1> : parameter is in the range of 0 to 1
  // alpha : name of the parameter
  // [nsub,2] : size of the parameter (number of rows, number of columns)
  real<lower=0,upper=1> alpha[nsub,2]; // learning rate - separate learning rates for stable and volatile; two per participant
  real<lower=0> beta[nsub];            // inverse temperature (i.e. how consistent choices are); one per participant
}

// This block runs the actual model
model {
  // temporary variables that we will compute for each person and each trial
  real predictionOpt1[ntr,nsub];  //prediction how likely option 1 is to be rewarded
  real predictionError[ntr,nsub]; // prediction error
  real utility1[ntr,nsub];        // utility of option 1
  real utility2[ntr,nsub];        // utility of option 2


  // Priors for inverse temperature (beta)
  // as no prior for alpha is defined, it implicitly becomes the range it is given in the parameters block, i.e. from 0 to 1
  beta  ~ normal(0,1);

  // The learning model: the aim is to define how the input data (i.e. the reward outcomes, the reward magnitudes) and parameters relate to the behavior
  // The basic structure of the model is exactly as in Matlab before:
  // The first lines define the learning of reward probabilities, then these are combined with magnitudes to give utilities
  // Then the choice utilities are linked to the actual choice using a softmax function
  for (is in 1:nsub){ // run the model for each subject
    // Learning
    predictionOpt1[1,is] = 0.5; // on the first trial, 50-50 is the best guess
    for (it in 1:(ntr-1)){
      predictionError[it,is]  = opt1Rewarded[it,is]-predictionOpt1[it,is];
      predictionOpt1[it+1,is] = predictionOpt1[it,is] + alpha[is,isStableBlock[it,is]]*(predictionError[it,is]);
    }
    // Decision - combine predictions of reward probability with magnitudes
    for (it in 1:ntr){
      if ((opt1Chosen[it,is]!=999) && includeTrial[it]==1){ // if there is no missing response and we want to fit the trial
        // Utility
        utility1[it,is] = predictionOpt1[it,is]*magOpt1[it,is];     // option 1: probability x magnitude
        utility2[it,is] = (1-predictionOpt1[it,is])*magOpt2[it,is]; // option 2: probability x magnitude

        // Compare the choice probability (based on the utility) to the actual choice
        // See the handout for the syntax of the bernoulli_logit function
        // equivalently we could have written (as we have done previously in Matlab; but this runs a bit less well in Stan).:
        // ChoiceProbability1[it,is] = 1/(1+exp(beta[is]*(util2[it,is]-util1[it,is]))); // the softmax is an 'inv_logit'
        // opt1Chosen[it,is] ~ bernoulli(ChoiceProbability1[it,is]);
        opt1Chosen[it,is] ~ bernoulli_logit(beta[is]*(utility1[it,is]-utility2[it,is])); // bernoulli is a distribution in the way as e.g. the 'normal distribution'
      }
    }
  }
}
