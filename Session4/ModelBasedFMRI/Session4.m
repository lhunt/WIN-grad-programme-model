%% Section 1: Understanding haemodynamic convolution (hrf)
% Make the hrf
window = 14; % how much time (in s) to display
TR     = 1;  % what the time between FMRI volumes was
sigma1 = 3;  % parameter for the shape of the hrf
my1    = 7;  % parameter for hrf shape 
alpha1=my1^2/sigma1^2;
beta1=my1/sigma1^2;
% Code the HRF
t=0:(TR):(window); % create the time window (from 0 s to 14s) for which to show the hrf
hrf = gammapdf(alpha1,beta1,t); % create the hrf 

% Plot the hrf shape
figure('name','Convolution','color',[1 1 1]);
subplot(2,1,1);hold on;
plot(hrf,'Linewidth',3);set(gca,'Fontsize',16);title('HRF function')


% Create an example set of events
event_times =   [50 100 150 160 170 180 190 200 303 306 309 312 315 330]; % onsets of the events
event_duration= [0  30  0   0   0   0   0   0    0  0    0   0   0 0]; % duration of events
event_values =  [1  1   2   1   1   1   1.5 2    1  1    1   1   1 -1]; % event values

% Create a vector that has for every time point whether there is an event
% or not (and the size and direction of the event)
nT = 400; %total number of timepoints in our fake FMRI data
regressor = zeros(nT,1); % the vector to be filled
for i = 1:length(event_times)
    regressor(event_times(i):event_times(i)+event_duration(i)) = event_values(i);
end
% Convolve the vector with the hrf
convolved_regressor = conv(regressor,hrf);

% Plot the resulting vector
subplot(2,1,2)
plot(regressor,'k','Linewidth',3);hold on;plot(convolved_regressor,'Linewidth',3)
legend('unconvolved regressor','convolved regressor');set(gca,'Fontsize',16);title('Convolved and unconvolved regressors')